import { useEffect } from 'react';
import BluetoothSerial from 'react-native-bluetooth-serial';
import { Buffer } from "buffer";
import iconv from 'iconv-lite';

// Event
const TYPE_EVENT_BLUETOOTH_ENABLED = 'TYPE_EVENT_BLUETOOTH_ENABLED';
const TYPE_EVENT_BLUETOOTH_DISABLED = 'TYPE_EVENT_BLUETOOTH_DISABLED';
const TYPE_EVENT_ERROR = 'TYPE_EVENT_ERROR';
const TYPE_EVENT_CONNECTION_LOST = 'TYPE_EVENT_CONNECTION_LOST';
// Action
const TYPE_BLUETOOTH_WRITE = 'BLUETOOTH_WRITE';
const TYPE_BLUETOOTH_WRITE_ERROR = 'BLUETOOTH_WRITE_ERROR';
const TYPE_BLUETOOTH_IS_CONNECTED = 'BLUETOOTH_IS_CONNECTED';
const TYPE_BLUETOOTH_IS_CONNECTED_ERROR = 'BLUETOOTH_IS_CONNECTED_ERROR';
const TYPE_BLUETOOTH_IS_ENABLED = 'BLUETOOTH_IS_ENABLED';
const TYPE_BLUETOOTH_IS_ENABLED_ERROR = 'BLUETOOTH_IS_ENABLED_ERROR';
const TYPE_BLUETOOTH_LIST = 'BLUETOOTH_LIST';
const TYPE_BLUETOOTH_LIST_ERROR = 'BLUETOOTH_LIST_ERROR';
const TYPE_BLUETOOTH_CONNECT = 'BLUETOOTH_CONNECT';
const TYPE_BLUETOOTH_CONNECT_ERROR = 'BLUETOOTH_CONNECT_ERROR';
const TYPE_BLUETOOTH_DISCONNECT = 'BLUETOOTH_DISCONNECT';
const TYPE_BLUETOOTH_DISCONNECT_ERROR = 'BLUETOOTH_DISCONNECT_ERROR';
const TYPE_BLUETOOTH_DISCOVER_UNPAIRED_DEVICES = 'BLUETOOTH_DISCOVER_UNPAIRED_DEVICES';
const TYPE_BLUETOOTH_DISCOVER_UNPAIRED_DEVICES_ERROR = 'BLUETOOTH_DISCOVER_UNPAIRED_DEVICES_ERROR';
const TYPE_BLUETOOTH_PAIR_DEVICE = 'BLUETOOTH_PAIR_DEVICE';
const TYPE_BLUETOOTH_PAIR_DEVICE_ERROR = 'BLUETOOTH_PAIR_DEVICE_ERROR';
const TYPE_BLUETOOTH_ON = 'TYPE_BLUETOOTH_ON';

function useBluetoothSerialEvent(onBluetoothEnabled, onBluetoothDisabled, onError, onConnectionLost) {
    useEffect(() => {
        BluetoothSerial.on('bluetoothEnabled', onBluetoothEnabled);
        BluetoothSerial.on('bluetoothDisabled', onBluetoothDisabled);
        BluetoothSerial.on('error', onError);
        BluetoothSerial.on('connectionLost', onConnectionLost);

        return () => {
            BluetoothSerial.removeListener('bluetoothEnabled', onBluetoothEnabled);
            BluetoothSerial.removeListener('bluetoothDisabled', onBluetoothDisabled);
            BluetoothSerial.removeListener('error', onError);
            BluetoothSerial.removeListener('connectionLost', onConnectionLost);
        };
    }, []);
}

function useBluetooth(webviewRef) {
    useEffect(() => {
        return () => {
            (async function() {
                const isConnected = await BluetoothSerial.isConnected();
                console.log('unmount isConnected', isConnected);
                if (isConnected) {
                    BluetoothSerial.disconnect();
                }
            })();
        };
    });

    const onMessage = (event) => {
        const data = JSON.parse(event.nativeEvent.data);
        console.log('onMessage', data);
        switch(data.type) {
            case TYPE_BLUETOOTH_WRITE:
                write(data.payload.message);
                break;
            case TYPE_BLUETOOTH_IS_CONNECTED:
                isConnected();
                break;
            case TYPE_BLUETOOTH_IS_ENABLED:
                isEnabled();
                break;
            case TYPE_BLUETOOTH_LIST:
                list();
                break;
            case TYPE_BLUETOOTH_CONNECT:
                connect(data.payload.deviceId);
                break;
            case TYPE_BLUETOOTH_DISCONNECT:
                disconnect();
                break;
            case TYPE_BLUETOOTH_DISCOVER_UNPAIRED_DEVICES:
                discoverUnpairedDevices();
                break;
            case TYPE_BLUETOOTH_PAIR_DEVICE:
                pairDevice(data.payload.deviceId);
                break;
        }
    };


    // Action
    function write(message) {
        const packetSize = 64;
        const toWrite = iconv.encode(message, 'Big5');
        const writePromises = [];
        const packetCount = Math.ceil(toWrite.length / packetSize);

        for (let i = 0; i < packetCount; i++) {
            const packet = new Buffer(packetSize);
            packet.fill(' ');
            toWrite.copy(packet, 0, i * packetSize, (i + 1) * packetSize);
            writePromises.push(BluetoothSerial.write(packet));
        }

        Promise.all(writePromises)
            .then((result) => {
                setTimeout(() => {
                    webviewRef.current.postMessage(JSON.stringify({
                        type: TYPE_BLUETOOTH_WRITE,
                        data: {
                            result,
                        },
                    }));
                }, 1000);
            })
            .catch((error) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_WRITE_ERROR,
                    error,
                }));
            });
    }

    function isConnected() {
        BluetoothSerial.isConnected()
            .then((isConnected) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_IS_CONNECTED,
                    data: {
                        isConnected,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_IS_CONNECTED_ERROR,
                    error,
                }));
            });
    }

    function isEnabled() {
        BluetoothSerial.isEnabled()
            .then((isEnabled) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_IS_ENABLED,
                    data: {
                        isEnabled,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_IS_ENABLED_ERROR,
                    error,
                }));
            });
    }

    function list() {
        BluetoothSerial.list()
            .then((devices) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_LIST,
                    data: {
                        devices,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_LIST_ERROR,
                    error,
                }));
            });
    }

    function connect(deviceId) {
        console.log('connect deviceId', deviceId);
        (async function() {
            const isConnected = await BluetoothSerial.isConnected();
            if (!isConnected) {
                BluetoothSerial.connect(deviceId)
                    .then((result) => {
                        webviewRef.current.postMessage(JSON.stringify({
                            type: TYPE_BLUETOOTH_CONNECT,
                            data: {
                                result,
                            },
                        }));
                    })
                    .catch((error) => {
                        webviewRef.current.postMessage(JSON.stringify({
                            type: TYPE_BLUETOOTH_CONNECT_ERROR,
                            error,
                        }));
                    });
            } else {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_CONNECT_ERROR,
                    message: 'already connect',
                }));
            }
        })();
    }

    function disconnect() {
        BluetoothSerial.disconnect()
            .then((result) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_DISCONNECT,
                    data: {
                        result,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_DISCONNECT_ERROR,
                    error,
                }));
            });
    }

    function discoverUnpairedDevices() {
        BluetoothSerial.discoverUnpairedDevices()
            .then((unpairedDevices) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_DISCOVER_UNPAIRED_DEVICES,
                    data: {
                        unpairedDevices,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_DISCOVER_UNPAIRED_DEVICES_ERROR,
                    error,
                }));
            });
    }

    function pairDevice(deviceId) {
        console.log('pairDevice', deviceId);
        BluetoothSerial.pairDevice(deviceId)
            .then((result) => {
                console.log('pairDevice', result);
                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_PAIR_DEVICE,
                    data: {
                        result,
                    },
                }));
            })
            .catch((error) => {
                console.log('pairDevice', error);

                webviewRef.current.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_PAIR_DEVICE_ERROR,
                    error,
                }));
            });
    }

    // Event
    const onBluetoothEnabled = () => {
        console.log('onBluetoothEnabled');
        webviewRef.current.postMessage(JSON.stringify({
            type: TYPE_EVENT_BLUETOOTH_ENABLED,
        }));
    };

    const onBluetoothDisabled = () => {
        console.log('onBluetoothDisabled');
        webviewRef.current.postMessage(JSON.stringify({
            type: TYPE_EVENT_BLUETOOTH_DISABLED,
        }));
    };

    const onError = (error) => {
        console.log('onError', error);
        webviewRef.current.postMessage(JSON.stringify({
            type: TYPE_EVENT_ERROR,
            error,
        }));
    };

    const onConnectionLost = () => {
        console.log('onConnectionLost');
        webviewRef.current.postMessage(JSON.stringify({
            type: TYPE_EVENT_CONNECTION_LOST,
        }));
    };

    useBluetoothSerialEvent(onBluetoothEnabled, onBluetoothDisabled, onError, onConnectionLost);

    return {
        onMessage,
    };
}

export { useBluetooth };
