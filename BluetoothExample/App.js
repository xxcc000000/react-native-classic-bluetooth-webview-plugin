import React, {
    useRef,
    useEffect,
} from 'react';
import {
    SafeAreaView,
    StatusBar,
    View,
    PermissionsAndroid,
    Alert,
} from 'react-native';
import { WebView } from 'react-native-webview';

import { useBluetooth } from './react-native-classic-bluetooth-webview-plugin';

const App = () => {
    const webviewRef = useRef(null);
    const { onMessage } = useBluetooth(webviewRef);

    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <WebView
                        source={{ uri: 'https://react-bluetoot-test.stackblitz.io' }}
                        ref={webviewRef}
                        javaScriptEnabled={true}
                        height={3}
                        useWebKit={true}
                        style={{ flex: 1 }}
                        onMessage={onMessage}
                    />
                </View>
            </SafeAreaView>
        </>
    );
};

export default App;
