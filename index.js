import { useEffect } from 'react';
import BluetoothSerial from 'react-native-bluetooth-serial';

// Event
const TYPE_EVENT_BLUETOOTH_ENABLED = 'TYPE_EVENT_BLUETOOTH_ENABLED';
const TYPE_EVENT_BLUETOOTH_DISABLED = 'TYPE_EVENT_BLUETOOTH_DISABLED';
const TYPE_EVENT_ERROR = 'TYPE_EVENT_ERROR';
const TYPE_EVENT_CONNECTION_LOST = 'TYPE_EVENT_CONNECTION_LOST';
// Action
const TYPE_BLUETOOTH_WRITE = 'BLUETOOTH_WRITE';
const TYPE_BLUETOOTH_WRITE_ERROR = 'BLUETOOTH_WRITE_ERROR';
const TYPE_BLUETOOTH_IS_CONNECTED = 'BLUETOOTH_IS_CONNECTED';
const TYPE_BLUETOOTH_IS_CONNECTED_ERROR = 'BLUETOOTH_IS_CONNECTED_ERROR';
const TYPE_BLUETOOTH_ON = 'TYPE_BLUETOOTH_ON';

function useBluetoothSerialEvent(onBluetoothEnabled, onBluetoothDisabled, onError, onConnectionLost) {
    useEffect(() => {
        BluetoothSerial.on('bluetoothEnabled', onBluetoothEnabled);
        BluetoothSerial.on('bluetoothDisabled', onBluetoothDisabled);
        BluetoothSerial.on('error', onError);
        BluetoothSerial.on('connectionLost', onConnectionLost);

        return () => {
            BluetoothSerial.removeListener('bluetoothEnabled', onBluetoothEnabled);
            BluetoothSerial.removeListener('bluetoothDisabled', onBluetoothDisabled);
            BluetoothSerial.removeListener('error', onError);
            BluetoothSerial.removeListener('connectionLost', onConnectionLost);
        };
    }, []);
}

function useBluetooth(webviewRef) {
    const onBluetoothEnabled = () => {
        webviewRef.postMessage(JSON.stringify({
            type: TYPE_EVENT_BLUETOOTH_ENABLED,
        }));
    };

    const onBluetoothDisabled = () => {
        webviewRef.postMessage(JSON.stringify({
            type: TYPE_EVENT_BLUETOOTH_DISABLED,
        }));
    };

    const onError = () => {
        webviewRef.postMessage(JSON.stringify({
            type: TYPE_EVENT_ERROR,
        }));
    };

    const onConnectionLost = () => {
        webviewRef.postMessage(JSON.stringify({
            type: TYPE_EVENT_CONNECTION_LOST,
        }));
    };

    useBluetoothSerialEvent(onBluetoothEnabled, onBluetoothDisabled, onError, onConnectionLost);

    webviewRef.onMessage = (event) => {
        webviewRef.onMessage();
        const data = JSON.parse(event.nativeEvent.data);
        switch(data.type) {
            case TYPE_BLUETOOTH_WRITE:
                write(data);
                break;
            case TYPE_BLUETOOTH_IS_CONNECTED:
                isConnected();
                break;
        }
    };

    function write(data) {
        BluetoothSerial.write(data)
            .then((result) => {
                webviewRef.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_WRITE,
                    data: {
                        result,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_WRITE_ERROR,
                    error,
                }));
            });
    }

    function isConnected() {
        BluetoothSerial.isConnected(data)
            .then((isConnected) => {
                webviewRef.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_IS_CONNECTED,
                    data: {
                        isConnected,
                    },
                }));
            })
            .catch((error) => {
                webviewRef.postMessage(JSON.stringify({
                    type: TYPE_BLUETOOTH_IS_CONNECTED_ERROR,
                    error,
                }));
            });
    }

    function on(eventName) {
        switch (eventName) {
            case 'bluetoothEnabled':
                onBluetoothEnabled();
                break;
            case 'bluetoothDisabled':
                onBluetoothDisabled();
                break;
            case 'error':
                onError();
                break;
            case 'connectionLost':
                onConnectionLost();
                break;
        }
    }

    function removeListener(eventName) {
        switch (eventName) {
            case 'bluetoothEnabled':
                BluetoothSerial.removeListener('bluetoothEnabled', onBluetoothEnabled);
                break;
            case 'bluetoothDisabled':
                BluetoothSerial.on('bluetoothDisabled', () => {
                    webviewRef.postMessage(JSON.stringify({
                        type: TYPE_EVENT_BLUETOOTH_DISABLED,
                    }));
                });
                break;
            case 'error':
                BluetoothSerial.on('error', () => {
                    webviewRef.postMessage(JSON.stringify({
                        type: TYPE_EVENT_ERROR,
                    }));
                });
                break;
            case 'connectionLost':
                BluetoothSerial.on('error', () => {
                    webviewRef.postMessage(JSON.stringify({
                        type: TYPE_EVENT_CONNECTION_LOST,
                    }));
                });
                break;
        }
    }
}

export { useBluetooth };
